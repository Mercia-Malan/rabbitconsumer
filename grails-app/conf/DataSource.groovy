dataSource {
    pooled = true
    driverClassName = "com.mysql.jdbc.Driver"
    url = "jdbc:mysql://localhost:3309/VAF5"
    dialect = 'org.hibernate.dialect.MySQL5InnoDBDialect'
    username = "SharedServices"
    password = "@No71a))"
    dbCreate = ""
}
hibernate {
    cache.use_second_level_cache = false
    cache.use_query_cache = false
    //cache.provider_class = "net.sf.ehcache.hibernate.EhCacheProvider"
    //naming_strategy = org.hibernate.cfg.DefaultNamingStrategy
}
// environment specific settings
environments {
    development {
        dataSource {

        }
    }
    test {
        dataSource {
        }
    }
    production {
        dataSource {

        }
    }
}
