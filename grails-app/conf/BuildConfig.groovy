grails.servlet.version = "2.5" // Change depending on target container compliance (2.5 or 3.0)
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.target.level = 1.6
grails.project.source.level = 1.6
grails.server.port.http = 8084
//grails.project.war.file = "target/${appName}-${appVersion}.war"

// uncomment (and adjust settings) to fork the JVM to isolate classpaths
//grails.project.fork = [
//   run: [maxMemory:1024, minMemory:64, debug:false, maxPerm:256]
//]

grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // specify dependency exclusions here; for example, uncomment this to disable ehcache:
        // excludes 'ehcache'
    }
    log "verbose" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    checksums true // Whether to verify checksums on resolve
//    legacyResolve false // whether to do a secondary resolve on plugin installation, not advised and here for backwards compatibility

    credentials {
        realm = "Sonatype Nexus Repository Manager"
        host = "nexus.conclude.co.za"
        username = "grails"
        password = "s7G!^dUTX9Xz"
    }

    repositories {
        inherits true // Whether to inherit repository definitions from plugins


        mavenRepo "https://nexus.conclude.co.za/nexus/content/groups/public"
        mavenRepo "https://nexus.conclude.co.za/nexus/content/groups/public-snapshots"
        mavenRepo "http://repo.grails.org/grails/core"
        mavenRepo "http://repo.grails.org/grails/repo"
        grailsRepo "https://grails.org/plugins"
        mavenRepo "https://maven.alfresco.com/nexus/content/groups/public/"
        grailsPlugins()
        grailsHome()
        grailsCentral()

        mavenRepo "http://repo.grails.org/grails/core"
        mavenRepo "http://repo.grails.org/grails/repo"
        mavenRepo 'http://repository.apache.org/snapshots'

        mavenLocal()
        mavenCentral()
    }

    dependencies {
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes e.g.
        compile "org.grails.plugins:shared-dao:2.8.38-SNAPSHOT"
        compile ("org.grails.plugins:shared-utils:1.0.16-SNAPSHOT") {
            excludes "shared-dao"
        }
        compile group: 'joda-time', name: 'joda-time', version: '2.3'

        // runtime 'mysql:mysql-connector-java:5.1.22'
    }

    plugins {
        runtime ":hibernate:$grailsVersion"
        runtime ":jquery:1.8.3"
        runtime ":resources:1.2"
        compile ":rabbitmq:1.0.0"
        // Uncomment these (or add new ones) to enable additional resources capabilities
        //runtime ":zipped-resources:1.0"
        //runtime ":cached-resources:1.0"
        //runtime ":yui-minify-resources:0.1.5"

        build ":tomcat:$grailsVersion"

        runtime ":database-migration:1.3.2"

        compile ':cache:1.0.1'
    }
}
