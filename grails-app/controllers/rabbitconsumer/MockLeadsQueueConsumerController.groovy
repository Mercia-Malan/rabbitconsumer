package rabbitconsumer

import groovy.json.JsonSlurper
import groovy.util.slurpersupport.GPathResult
import groovy.xml.StreamingMarkupBuilder
import org.joda.time.DateTime
import org.springframework.amqp.core.Message
import za.co.signio.model.Deal
import za.co.signio.model.Dealstate
import za.co.signio.model.Dealtype


class MockLeadsQueueConsumerController {

    //----------------------------------------------------//
    def rabbitTemplate

    def queueName = "LEADS_DELAY"
    //----------------------------------------------------//
    def receiveMessage = {
        rabbitTemplate.receive(queueName)
    }

    def index() {

        int count = 0;
        BufferedWriter bw = null;
        FileWriter fw = null;

        fw = new FileWriter("C:\\data\\state3.txt", true);
        bw = new BufferedWriter(fw)
        def dupes = []
        while (count < 40) {

            Message message = receiveMessage()
            def error = false
            def alreadySent = false;
            def duplicate = false;
            String output = ""

            if (message != null) {
                String messageBody = new String(message.getBody())
                String batchref = "none"

                try {
                    Map parameterMap = (Map) new JsonSlurper().parseText(messageBody)
                    def messageBatchNumber = parameterMap["batchNumber"] as int
                    def messageSDM = parameterMap["signioDataModel"].toString()

                    def signioDataModel = new XmlSlurper().parseText(messageSDM)

                    LeadsHelper.getListOfValuesFromSDM(signioDataModel, "lead").each { lead ->
                        //----------------------------------------------------//
                        Map subFields = lead.subField.findAll().collectEntries {
                            [(it.@formFieldName.text()): it.text()]
                        }

                        //----------------------------------------------------//
                        def batchRef = subFields.get("leadBatchRef").toString()
                        def dealStateCode = subFields.get("leadDelayDealStateCode") as int

                        //----------------------------------------------------//
                        Deal deal = Deal.findByBatchRef(batchRef)

                        def state = deal.dealstate.stateCode
                        def id = deal.id
                    }


                    batchref = LeadsHelper.getSubFieldValueFromSDM(signioDataModel, "lead", "leadBatchRef")
                    def dealStateCode = LeadsHelper.getSubFieldValueFromSDM(signioDataModel, "lead", "leadDealStateCode") as int
                    def leadFirstProcessedTime = LeadsHelper.getSubFieldValueFromSDM(signioDataModel, "lead", "leadFirstProcessedTime")

                    def parentId = LeadsHelper.getSingleValueFromSDM(signioDataModel, "parentId") as int

                    def s = parentId + "|" + batchref
                    if(dupes.contains(s)) {
                        duplicate = true
                    }
                    else {
                        dupes.add(s)
                    }
                    output = messageBatchNumber + "|" + parentId + "|" + batchref + "|" + dealStateCode + "|" + leadFirstProcessedTime
                    if (dealStateCode == 185100) {
                        error = true
                        log.info("dont send")
                    }
                    else if (dealStateCode == 185200) {


                        def leadDealType = Dealtype.findByType("Lead")
                        def deals = Deal.findAllByParentIdAndDealtypeNotEqual(parentId, leadDealType)

                        //----------------------------------------------------//
                        if (deals != null) {
                            boolean isApproved = deals.any { deal ->
                                isDealApprovedState(deal.dealstate.stateCode)
                            }

                            output += " |approved: " + isApproved
                            log.info(output)
//                            output =  count + " | " + parentId + " | " + messageBatchNumber + "| batchRef | " + batchref + " | Has approved deals: " + isApproved

                            boolean allDeclined = deals.every { deal ->
                                deal.dealstate.stateCode == 23220 || deal.dealstate.stateCode == 24000
                            }

                            if (allDeclined) {
                                log.info("all declined")
                                updateAllLeadsWithDelayDealState(signioDataModel, false)
                            }
                            else if (isApproved) {
                                //----------------------------------------------------//
                                LeadsHelper.getListOfValuesFromSDM(signioDataModel, "lead").each { lead ->
                                    Map subFields = lead.subField.findAll().collectEntries {
                                        [(it.@formFieldName.text()): it.text()]
                                    }
                                    //----------------------------------------------------//
                                    def delayDealStateCode = subFields.get("leadDelayDealStateCode") as int
                                    def innerDealStateCode = subFields.get("leadDealStateCode") as int
                                    def leadBatchRef = subFields.get("leadBatchRef").toString()

                                    //----------------------------------------------------//
                                    if (isDelayedState(delayDealStateCode)) {

                                        //----------------------------------------------------//
                                        DateTime currentDateTime = new DateTime()
                                        String leadEndTime = currentDateTime.toString()
                                        log.info("Lead delay end time created: " + leadEndTime.toString())

                                        //----------------------------------------------------//
                                        lead.appendNode {
                                            subField(formFieldName: "leadDelayStart", mapping: 'null', currentDateTime.toString())
                                        }
                                        lead.appendNode {
                                            subField(formFieldName: "leadDelayEnd", mapping: 'null', leadEndTime)
                                        }
                                    }

                                    //----------------------------------------------------//
                                    if (delayDealStateCode != innerDealStateCode) {
                                        lead.subField.find {
                                            it.@formFieldName.text().equals("leadDealStateCode")
                                        }.replaceNode {}
                                        lead.appendNode {
                                            subField(formFieldName: "leadDealStateCode", mapping: 'null', delayDealStateCode)
                                        }
                                    }

                                    //----------------------------------------------------//
                                    def newSdmString = new StreamingMarkupBuilder().bind {
                                        mkp.yield signioDataModel
                                    }.toString()

                                    //----------------------------------------------------//
                                    //----------------------------------------------------//
                                    def newSdm = new XmlSlurper().parseText(newSdmString)

                                    //----------------------------------------------------//
                                    def current = LeadsHelper.getListOfValuesFromSDM(newSdm, "lead")

                                    //----------------------------------------------------//
                                    current.eachWithIndex { currentLead, index ->
                                        if (!(currentLead.subField.find { it.@formFieldName == "leadBatchRef" }?.text()).equals(leadBatchRef)) {
                                            newSdm.dataBundle.field.findAll { it.@formFieldName == "lead" }[index].replaceNode {}
                                        }
                                    }

                                    //----------------------------------------------------//
                                    def newSdmStringSingleLead = new StreamingMarkupBuilder().bind {
                                        mkp.yield newSdm
                                    }.toString()

                                    rabbitSend("LEADS_OUTGOING", newSdmStringSingleLead)
                                }
                            }
                            else {
                                rabbitSend(queueName, messageBody)
                            }
                        }
                    }
                    else {
                        log.info("not waiting for approval - " + dealStateCode)
//                        output = "not waiting for approval - " + dealStateCode
                        rabbitSend(queueName, messageBody)
                    }
                }
                catch (Exception e) {
                    log.info(batchref + " | exception " + e)
                    output = batchref + " | exception " + e
                    if (!error) {
                        rabbitSend(queueName, messageBody)
                    }
                }

//                if (!duplicate) {
//                    rabbitSend(queueName, messageBody)
//                }

                try {
                    bw.writeLine(output)
                } catch (IOException e) {

                    e.printStackTrace();

                }
            }
            count++;
        }

        if (bw != null)
            bw.close();

        if (fw != null)
            fw.close();

    }


    private static void updateAllLeadsWithDelayDealState(GPathResult signioDataModel, boolean dealApproved) {

        signioDataModel.dataBundle.field.findAll { field -> field.@formFieldName.text().equals("lead") }.each{ lead ->
            //----------------------------------------------------//
            Map subFields = lead.subField.findAll().collectEntries {
                [(it.@formFieldName.text()): it.text()]
            }

            //----------------------------------------------------//
            def batchRef = subFields.get("leadBatchRef").toString()
            def dealStateCode = subFields.get("leadDelayDealStateCode") as int

            //----------------------------------------------------//
            Deal deal = Deal.findByBatchRef(batchRef)

            deal.dealstate = dealApproved ?
                    Dealstate.findByStateCode(dealStateCode) :
                    Dealstate.findByStateCode(184000)

            deal.dateUpdated = new Date()
            deal.save(failOnError: true,flush: true)
        }
    }

        public static boolean isDelayedState(int code) {
            return (code == 180001) || (code = 180210) || (code == 180230) || (code == 180250) || (code == 180220)
        }

        public static boolean isDealApprovedState(int code)
    {
        int contractRangeStart = 30000;
        int contractRangeEnd = 39999;

        int podiumApproved3rdParty = 23210;

        int payoutReceived = 53100;

        return code == 23900 || code == podiumApproved3rdParty || code == payoutReceived || (code > contractRangeStart && code < contractRangeEnd);
    }
}
