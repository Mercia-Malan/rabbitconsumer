package rabbitconsumer

import groovy.util.slurpersupport.GPathResult
import groovy.xml.StreamingMarkupBuilder
import org.apache.log4j.Logger


class LeadsHelper {

    //----------------------------------------------------//
    static String leadDealType = "Lead"
    static leadsQueue = 'LEADS_QUEUE'
    static leadsDelayQueue = 'LEADS_DELAY'
    static leadsOutgoingQueue = 'LEADS_OUTGOING'
    static int ApprovalPendingIndicator = 0
    static int ApprovalAllDeclinedIndicator = 1
    static int ApprovalApprovedIndicator =  2
    static String canSendIndicator = "canSendLead"
    static String signioDataModelIndicator = "signioDataModel"
    static String batchNumberIndicator = "batchNumber"
    static String leadDealStateIndicator = "leadDealState"
    static String leadDealStateCommentIndicator = "leadComment"
    static String yesIndicator = "Yes"
    static String leadsDeliveryMethodEmail = "EMAIL"
    static String leadsDeliveryMethodFtp = "FTP"
    static String leadsDeliveryMethodWebservice = "WS"
    static String shouldRetryIndicator = "shouldRetry"
    static String leadTransformedXmlIndicator = "transformedXML"
    static String wsResponseXmlIndicator = "wsResponseXML"
    static String logMessage = "logMessage"

    //----------------------------------------------------//
    private static final Logger LOGGER = Logger.getLogger(this)


    //----------------------------------------------------//
    static String buildNewSDMStringWithSingleLeadSection(String newSdmString, String leadBatchRef)
    {
        //----------------------------------------------------//
        def newSdm = new XmlSlurper().parseText(newSdmString)

        //----------------------------------------------------//
        def current = getListOfValuesFromSDM(newSdm, "lead")

        //----------------------------------------------------//
        current.eachWithIndex { currentLead, index ->
            if (!(currentLead.subField.find { it.@formFieldName == "leadBatchRef" }?.text()).equals(leadBatchRef)) {
                newSdm.dataBundle.field.findAll { it.@formFieldName == "leadDelayDealStateCode" }[index].replaceNode {}
            }
        }

        //----------------------------------------------------//
        def newSdmStringSingleLead = new StreamingMarkupBuilder().bind {
            mkp.yield newSdm
        }.toString()

        //----------------------------------------------------//
        return newSdmStringSingleLead
    }

    //----------------------------------------------------//
    static getSingleValueFromSDM(GPathResult signioDataModel, String fieldName)
    {
        return signioDataModel.dataBundle.field.find { field -> field.@formFieldName.text().equals(fieldName) }?.text()
    }

    //----------------------------------------------------//
    static getListOfValuesFromSDM(GPathResult signioDataModel, String fieldName)
    {
        return signioDataModel.dataBundle.field.findAll { field -> field.@formFieldName.text().equals(fieldName) }
    }

    //----------------------------------------------------//
    static getSubFieldValueFromSDM(GPathResult signioDataModel, String fieldName, String subFieldName) {
        return signioDataModel.dataBundle.field
                .find { field -> field.@formFieldName.text().equals(fieldName) }
                .subField.find { subField -> subField.@formFieldName.text().equals(subFieldName)}.text()
    }

}
