package rabbitconsumer

import za.co.signio.model.Deal
import za.co.signio.model.Dealtype

class DealAnalysisController {

    def index() {
        BufferedWriter bw = null;
        FileWriter fw = null;

        fw = new FileWriter("C:\\data\\leads.txt", true);
        bw = new BufferedWriter(fw)
        Dealtype dealtype = Dealtype.findById(1);
        Dealtype lead = Dealtype.findById(22);
        long maxId = 10997925
        long startId = maxId - 250000
        long endId = maxId - 249000
        def deals = Deal.findAllByDealtypeAndIdBetween(dealtype, startId, endId)

        deals.each { deal ->
            def signioDataModel = new XmlSlurper().parseText(deal.originalXml)
            def leads = signioDataModel.dataBundle.field.findAll { field -> field.@formFieldName.text().equals("lead") }
            if (leads != null && leads != "") {
                def consent = signioDataModel.dataBundle.field.find { field -> field.@formFieldName.text().equals("leadsConsentIndicator") }.text()
                def consent2 = signioDataModel.dataBundle.field.find { field -> field.@formFieldName.text().equals("marketingConsentIndicator") }.text()
                def delay = signioDataModel.dataBundle.field.find { field -> field.@formFieldName.text().equals("leadDelay") }.text()
                def saveSubmit = signioDataModel.dataBundle.field.find { field -> field.@formFieldName.text().equals("saveOrSubmit") }.text()

                if (consent == "No" && consent2 == "No" && delay == "180001" && saveSubmit == "submit") {
                    def foundLeads = Deal.findByDealtypeAndParentId(lead, deal.id as int)
                    if (foundLeads != null) {
                        def string = "Deal: ${deal.id} - date: ${deal.dateCreated} - consent: ${consent} - delay: ${delay}"
                        log.info(string)
//                        try {
//                            bw.writeLine(string)
//                        } catch (IOException e) {
//
//                            e.printStackTrace();
//                        }

                    }
                }
            }
        }

        if (bw != null)
            bw.close();

        if (fw != null)
            fw.close();
    }
}
