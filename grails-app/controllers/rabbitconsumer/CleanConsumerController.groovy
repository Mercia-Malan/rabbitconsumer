package rabbitconsumer

import groovy.json.JsonSlurper
import org.springframework.amqp.core.Message
import za.co.signio.model.Deal

class CleanConsumerController {

    //----------------------------------------------------//
    def rabbitTemplate

    def queueName = "LEADS_DELAY"
    //----------------------------------------------------//
    def receiveMessage = {
        rabbitTemplate.receive(queueName)
    }

    def NUMBER_MESSAGES_ON_QUEUE = 850

    def index() {
        int count = 0;

        while (count < NUMBER_MESSAGES_ON_QUEUE) {

            Message message = receiveMessage()
            def error = false

            if (message != null) {
                String messageBody = new String(message.getBody())

                try {
                    Map parameterMap = (Map) new JsonSlurper().parseText(messageBody)
                    def messageSDM = parameterMap["signioDataModel"].toString()

                    def signioDataModel = new XmlSlurper().parseText(messageSDM)
                    def parentId = LeadsHelper.getSingleValueFromSDM(signioDataModel, "parentId") as int

                    log.info("parent: " + parentId)
                    if(parentId == 11589255)
                    {
                        LeadsHelper.getListOfValuesFromSDM(signioDataModel, "lead").each { lead ->
                            //----------------------------------------------------//
                            Map subFields = lead.subField.findAll().collectEntries {
                                [(it.@formFieldName.text()): it.text()]
                            }

                            //----------------------------------------------------//
                            def batchRef = subFields.get("leadBatchRef").toString()
                            def dealStateCode = subFields.get("leadDelayDealStateCode") as int

                            //----------------------------------------------------//
                            Deal deal = Deal.findByBatchRef(batchRef)

                            def state = deal.dealstate.stateCode
                            def id = deal.id
                        }
                    }

                    //put message back on queue
                    rabbitSend(queueName, messageBody)
                }
                catch (Exception e) {
                    rabbitSend(queueName, messageBody)
                }
            }
            count++
        }

    }
}
