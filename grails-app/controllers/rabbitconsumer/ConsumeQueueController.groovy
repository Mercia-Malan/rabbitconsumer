package rabbitconsumer

import grails.converters.JSON
import groovy.json.JsonSlurper
import groovy.xml.StreamingMarkupBuilder
import org.joda.time.DateTime
import org.springframework.amqp.core.Message


class ConsumeQueueController {

    //----------------------------------------------------//
    def rabbitTemplate

    def queueName = "LEADS_DELAY"
    //----------------------------------------------------//
    def receiveMessage = {
        rabbitTemplate.receive(queueName)
    }

    def index() {

        int count = 0;

        while (count < 850) {


            Message message = receiveMessage()
            def error = false
            def old = false

            if (message != null) {
                String messageBody = new String(message.getBody())

                Map parameterMap = (Map) new JsonSlurper().parseText(messageBody)
                def messageBatchNumber = parameterMap["batchNumber"] as int
                def messageSDM = parameterMap["signioDataModel"].toString()

                def signioDataModel = new XmlSlurper().parseText(messageSDM)
                def parentId = signioDataModel.dataBundle.field.find { field -> field.@formFieldName.text().equals("parentId") }.text()
                def leads = signioDataModel.dataBundle.field.findAll { field -> field.@formFieldName.text().equals("lead") }
                def consent = signioDataModel.dataBundle.field.find { field -> field.@formFieldName.text().equals("leadsConsentIndicator") }.text()
                def consent2 = signioDataModel.dataBundle.field.find { field -> field.@formFieldName.text().equals("marketingConsentIndicator") }.text()
//            log.info("messageId: " + parentId)

                if (consent == "No") {
//                    error = true
                    log.info("Consent not given")
                }
                if (leads != null && leads != "") {
                    def statecode = signioDataModel.dataBundle.field
                            .find { field -> field.@formFieldName.text().equals("lead") }
                            .subField.find { subField -> subField.@formFieldName.text().equals("leadDealStateCode") }.text()

                    def batchRef = signioDataModel.dataBundle.field
                            .find { field -> field.@formFieldName.text().equals("lead") }
                            .subField.find { subField -> subField.@formFieldName.text().equals("leadBatchRef") }.text()

                    def delaystatecode = signioDataModel.dataBundle.field
                            .find { field -> field.@formFieldName.text().equals("lead") }
                            .subField.find { subField -> subField.@formFieldName.text().equals("leadDelayDealStateCode") }.text()

                    def start = signioDataModel.dataBundle.field
                            .find { field -> field.@formFieldName.text().equals("lead") }
                            .subField.find { subField -> subField.@formFieldName.text().equals("leadDelayStart") }.text()

                    def leadFirstProcessedTime = signioDataModel.dataBundle.field
                            .find { field -> field.@formFieldName.text().equals("lead") }
                            .subField.find { subField -> subField.@formFieldName.text().equals("leadFirstProcessedTime") }.text()

                    log.info(count + " | " + parentId + " | " + messageBatchNumber + "| batchRef | " + batchRef + "| leadconsent | " + consent +  " | leads section exists: |" + statecode + " | " + delaystatecode + " | " + leadFirstProcessedTime)
//                    log.info(batchRef)

//                    def date = new Date(leadFirstProcessedTime as String)
                    if (leadFirstProcessedTime == "") {
                        log.info("old");
                        old = true
                    }
                    else {
                        def date = DateTime.parse(leadFirstProcessedTime)
                        def minDate = new DateTime("2017-11-23T00:00:56.013+02:00")
                        if (date < minDate) {
                            log.info("old");
                            old = true
                        }
                    }

                    if (statecode == "185100") {
//                        error = true
                        log.info("dont send")
                    }
                    if (batchRef == "BF2FP19692") {
                        log.info("found missing batchref")
//                        error = true
//                        leads.each { lead ->
//                            lead.subField.find { subField -> subField.@formFieldName.text().equals("leadDelayDealStateCode") }.replaceNode {
//                                subField(formFieldName: "leadDealStateCode", mapping: 'null', 180001)
//                            }
//                        }
//                        lead.appendNode {
//                            subField(formFieldName: "leadDelayStart", mapping: 'null', "2017-12-06 14:31")
//                        }
//                        lead.appendNode {
//                            subField(formFieldName: "leadDelayEnd", mapping: 'null',  "2017-12-06 14:31")
//                        }
//
//                        String updatedSDMString = new StreamingMarkupBuilder().bind {
//                            mkp.yield signioDataModel
//                        }.toString()
//
//                        parameterMap["signioDataModel"] = updatedSDMString
//
//                        messageBody = (parameterMap as JSON).toString()
//                        log.info("set new messagebody")
//                    error = true
                    }


                    if (delaystatecode == "") {
                        leads.each { lead ->
                            lead.subField.find { subField -> subField.@formFieldName.text().equals("leadDelayDealStateCode") }.replaceNode {
                                subField(formFieldName: "leadDelayDealStateCode", mapping: 'null', 180001)
                            }
                        }

                        String updatedSDMString = new StreamingMarkupBuilder().bind {
                            mkp.yield signioDataModel
                        }.toString()

                        parameterMap["signioDataModel"] = updatedSDMString

                        messageBody = (parameterMap as JSON).toString()
                        log.info("set new messagebody")
                    }

                    if (delaystatecode == "180201") {
                        log.error("wrong state")
                        error = true
                    }

                } else {
                    log.info("leads empty " + parentId)
//                if(parentId == "8399974") {
//                    log.error("this one")
                    error = true
//                }
                }


//                parameterMap["batchNumber"] = "0"
//                messageBody = (parameterMap as JSON).toString()
//                log.info("reset batchNumber")

                if (error == false && !old) {
//                log.info("error: " + error)
                    rabbitSend(queueName, messageBody)
                }
//            log.info(" -------------------------------- ")

            }
            count++;
        }

    }
}
